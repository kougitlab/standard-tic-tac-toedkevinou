def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()
#first edit, refactor.
def game_over(board, i):
    print_board(board)
    print("GAME OVER")
    print(board[i], "has won")
    exit()
#created function if top row is winner.   
def is_row_is_winner(board, row_number):
    row_pos_1 = board[3 * row_number - 3]
    row_pos_2 = board[(3 * row_number - 3) + 1]
    row_pos_3 = board[(3 * row_number - 3) + 2]
    return row_pos_1 == row_pos_2 and row_pos_2 == row_pos_3
def is_column_is_winner(board, column_number):
    column_pos_1 = board[column_number - 1]
    column_pos_2 = board[column_number +2]
    column_pos_3 = board[column_number +5]
    return column_pos_1 == column_pos_2 and column_pos_2 == column_pos_3
def is_diagonal_is_winner(board, diagonal_number):
    diagonal_number_1 = board[diagonal_number]

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_is_winner(board, 1):
        #we replaced the repetitive if statement, and added our own function game_over.
        game_over(board, 0)
    elif is_row_is_winner(board, 2):
        game_over(board, 3)
    elif is_row_is_winner(board, 3):
        game_over(board, 6)
    #need column function, for winner
    elif is_column_is_winner(board, 1):
        game_over(board, 0)
    elif is_column_is_winner(board, 2):
        game_over(board, 1)
    elif is_column_is_winner(board, 3):
        game_over(board, 2)
    #need function for is_diagonal_is_winner
    elif board[0] == board[4] and board[4] == board[8]:
        game_over()
    elif board[2] == board[4] and board[4] == board[6]:
        game_over()

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
